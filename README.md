# Armageddon MC-Modpack

The modpack is (not yet) available on [technicpack.net](https://www.technicpack.net/)

## About this modpack
Armageddon is a heavily technical oriented modpack. You'll start on a post-apocalyptic earth during night-time in you vault.

Due the loss of the ozone-layer, you'll burn during the day without a solar protecting equipment.
So for the beginning, the only "save" time to exit your vault is during the night. But as you might know as a minecraft-veteran, during the night, hostile mobs are spawning.

This modpack is aimed to minecraft-players who have already a big knowledge and survial skill of this game.

## Active Mods
This list is representing the currently active mods in the modpack.

* Alchemistry
* Alib
* [Angrysun](https://github.com/LinuxSquare/AngrySun)`*`
* AppliedEnergistics 2
* BadMobs
* BiomesOPlenty
* ChickenChunks
* CodeChickenLib
* Coroutil
* CraftTweaker2
* CreativeCore
* DefaultWorldGenerator-port
* DrCyanosLootableBodies
* EnderCore
* EnderIO
* EnhancedVisuals
* ExtraPlanets
* Forgelin
* ForgeMultipart
* Galacticraft Planets
* Galacticraft Core
* iChunUtil
* InventoryTweaks
* IronChest
* JAOPCA
* JEI
* JustEnoughButton
* LostCities
* Mantle
* Mekansim
* MekanismGenerators
* MicdoodleCore
* MiniHUD
* MJRLegendsLib
* MobDismemberment
* MoBends
* More Planets
* More Planets Extras
* OptiFine
* Patchouli
* Planet Progression
* Portal Gun
* ResourceLoader-MC
* SteveKunG's-Lib
* Torohealth
* Travelers Backpack
* VoxelMap
* Wastelands`*`
* Weather2
* World-Tooltips
* WR-CBE (Wireless Redstone)
* WrapUp

`Mods marked with * have been edited to fit in this modpack`