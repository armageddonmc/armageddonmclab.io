# Tips 'n Tricks

## Solar protective cloak & hood
Always make sure, you have enough supplies to craft a solar protective cloak and hood.
It needs alot of strings (56 pcs. to be exact), so a spider farm isn't a bad idea. 

(Maybe use a powered spawner from `Ender IO` in combination with a broken spider spawner to make an automated trap)

## Leaving earth for space exploration
It's recommended to have a spare or active protective cloak & hood on your armour or separately in your inventory / backpack.

You won't need them on the planets/moons other than the earth. But if you want to return to the earth, it's a good idea, as you won't have an option to look up the local time on earth.
 
It could happen, that you land during the day. So if you don't have an active solar protecting cloak and hood, you will start to burn immediately while gliding down with your parachute and probably die.

(It's not recommended to unequip your parachute and try landing by slowing down using your jetpack... You'll probably end up dying)

### Crafting Stitched Reed on the moon
There is however the possibility to craft stitched reed with galacticraft materials.
For example instead of using standard iron ingots, you can use meteoric iron ingots (somehow sort of iron), but by using the alternate recipe, you will only get 1 instead of the default 2 stitched reed.
 
The same procedure for the iron saw of microblocks. You can use meteoric iron ingots instead of standard iron ingots to craft an iron saw.
 
However, iron ingots can be found in the moon dungeons. 
So if you don't want to waste meteoric iron, look out for moon-dungeons to gather some.